inherit autotools

DESCRIPTION = "Tinyalsa Library"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/\
${LICENSE};md5=3775480a712fc46a69647678acb234cb"
PR = "r1"

SRCREV = "046f98c7caa5457edd520ad7ebbe8b4f394d2837"
SRC_URI = "git://codeaurora.org/quic/le/platform/external/tinyalsa.git;protocol=git;branch=github/master \
           file://Makefile.am \
           file://configure.ac \
           file://tinyalsa.pc.in \
           file://0001-tinyalsa-add-support-for-PCM-plugins.patch \
           file://0001-tinyalsa-add-support-for-mixer-plugins.patch \
           file://0001-Add-support-for-event-handling.patch \
           file://0001-Add-PCM_FORMAT_INVALID-constant.patch \
           file://0001-Fix-compilation-for-LE-platform.patch"

S = "${WORKDIR}"

EXTRA_OEMAKE = "DEFAULT_INCLUDES=-I${WORKDIR}/git/include/"

DEPENDS = "libcutils"
